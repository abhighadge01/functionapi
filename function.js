const express=require('express');
const mode=express.Router();

function logInfo(req,res,next)
{
    console.log("Hello from logInfo() middleware...");
    next();

};
function addData(req,res,next)
{
   var person={
     name:"Abhijit Ghadge",
     city:"Pune"
   };
   req.person=person;
   next();
}
mode.use(logInfo);
mode.use(addData);


mode.get('/',function(req,res){
res.send("Welcome to Pune");
});


mode.get('/person ',function(req,res){
  res.send('Name:'+ req.person.name +',City:'+ req.person.city);
  });
module.exports=mode;
